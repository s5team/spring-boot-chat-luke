package rethinkdb.db;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RethinkDBConfiguration {
    // connect to docker
    public static final String DBHOST = "127.0.0.1";//Docker内部 192.168.99.100

    @Bean
    public RethinkDBConnectionFactory connectionFactory() {
        return RethinkDBConnectionFactory.builder().host(DBHOST).build();
    }

    @Bean
    DbInitializer dbInitializer() {
        return new DbInitializer();
    }
}
