package rethinkdb.db;

import com.rethinkdb.RethinkDB;
import com.rethinkdb.net.Connection;
import com.rethinkdb.net.ConnectionInstance;
import lombok.Builder;
import lombok.Data;

import java.util.concurrent.TimeoutException;
@Data
@Builder
public class RethinkDBConnectionFactory {
    private String host;
    public Connection<ConnectionInstance> createConnection() {
        try {
            return RethinkDB.r.connection().hostname(host).connect();
        } catch (TimeoutException e) {
            throw new RuntimeException(e);
        }
    }
}
