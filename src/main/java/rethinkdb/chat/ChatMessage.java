package rethinkdb.chat;

import lombok.Data;

import java.time.OffsetDateTime;

@Data
public class ChatMessage {
    public String message;
    public String from;
    public OffsetDateTime time;
}
